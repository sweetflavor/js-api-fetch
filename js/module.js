let dataUrl = 'https://api.chucknorris.io/jokes/random';

// const dataLoad = fetch(dataUrl)
//     .then(
//         function (response) {
//             if (response.status !== 200) {
//                 console.log('Looks like there was a problem. Status Code: ' +
//                     response.status);
//                 return;
//             }

//             // Examine the text in the response
//             response.json().then(function (data) {
//                 console.log(data);
//             });
//         }
//     )
//     .catch(function (err) {
//         console.log('Fetch Error :-S', err);
//     });

// module.exports = dataUrl;

export const getData = () => fetch(dataUrl)
    .then(response => response.json())